# importing required libraries and packages
import uvicorn
import fastapi
from typing import List, Optional
from fastapi import FastAPI, Query
from pydantic import BaseModel
from Filter import filterProcess
from Aggregation import aggregationProcess

# assigning FastAPI as app
app = FastAPI()


# Defining Base Model
class FilterRequest(BaseModel):
    # Input file name
    fileName: "str"
    # Expression to filter entries (ie. <, >, =, etc.)
    expression: "str"
    # Output file name
    outputFile: "str"


# Defining Base Model
class AggregationRequest(BaseModel):
    # Input file name
    fileName: "str"
    # Column Name to group by
    groupColumnName: "str"
    # Expression to filter entries (ie. <, >, =, etc.)
    expression: "list"
    # Column name to perform aggregation function on
    aggColumnName: "str"
    # Output file name
    outputFile: "str"


@app.post("/FilterAPI")
# calling the functions with given inputs
async def FilterAPI(LR1: FilterRequest):
    Result = filterProcess(LR1.fileName, LR1.expression, LR1.outputFile)
    # ...
    # Runs the Filter Process
    # ...
    return {f"{Result}"}


@app.post("/AggregationAPI")
# calling the functions with given inputs
async def AggregationAPI(LR1: AggregationRequest):
    Result = aggregationProcess(LR1.fileName, LR1.groupColumnName, LR1.expression, LR1.aggColumnName, LR1.outputFile)
    # ...
    # Runs the Aggregation Process
    # ...
    return {f"{Result}"}
