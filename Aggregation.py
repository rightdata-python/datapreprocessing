from pyspark.sql import SparkSession
import pandas as pd
import pyspark.sql.functions as f

spark = SparkSession.builder.getOrCreate()
print("SparkSession Starter")


def aggregationProcess(fileName, groupColumnName, expression, aggColumnName, outputFile):
    df = spark.read.option("header", True).csv(fileName)
    if 'approx_count_distinct' in expression:
        df = df.groupby([groupColumnName]).agg(f.approx_count_distinct(aggColumnName))
    if 'avg' in expression:
        df = df.groupby([groupColumnName]).agg(f.avg(aggColumnName))
    if 'collect_list' in expression:
        df = df.groupby([groupColumnName]).agg(f.collect_list(aggColumnName))
    if 'collect_set' in expression:
        df = df.groupby([groupColumnName]).agg(f.collect_set(aggColumnName))
    if 'countDistinct' in expression:
        df = df.groupby([groupColumnName]).agg(f.countDistinct(aggColumnName))
    if 'count' in expression:
        df = df.groupby([groupColumnName]).agg(f.count(aggColumnName))
    if 'grouping' in expression:
        df = df.groupby([groupColumnName]).agg(f.grouping(aggColumnName))
    if 'first' in expression:
        df = df.groupby([groupColumnName]).agg(f.first(aggColumnName))
    if 'last' in expression:
        df = df.groupby([groupColumnName]).agg(f.last(aggColumnName))
    if 'kurtosis' in expression:
        df = df.groupby([groupColumnName]).agg(f.kurtosis(aggColumnName))
    if 'max' in expression:
        df = df.groupby([groupColumnName]).agg(f.max(aggColumnName))
    if 'min' in expression:
        df = df.groupby([groupColumnName]).agg(f.min(aggColumnName))
    if 'mean' in expression:
        df = df.groupby([groupColumnName]).agg(f.mean(aggColumnName))
    if 'skewness' in expression:
        df = df.groupby([groupColumnName]).agg(f.skewness(aggColumnName))
    if 'stddev' in expression:
        df = df.groupby([groupColumnName]).agg(f.stddev(aggColumnName))
    if 'stddev_samp' in expression:
        df = df.groupby([groupColumnName]).agg(f.stddev_samp(aggColumnName))
    if 'stddev_pop' in expression:
        df = df.groupby([groupColumnName]).agg(f.stddev_pop(aggColumnName))
    if 'sum' in expression:
        df = df.groupby([groupColumnName]).agg(f.sum(aggColumnName))
    if 'sumDistinct' in expression:
        df = df.groupby([groupColumnName]).agg(f.sumDistinct(aggColumnName))
    if 'variance' in expression:
        df = df.groupby([groupColumnName]).agg(f.variance(aggColumnName))
    dataset = df.toPandas()  # converting to pandas dataframe
    dataset.to_csv(outputFile)


# aggregationProcess("IRIS.csv", 'species', 'avg', "sepal_length", "IRISoutputagg.csv")


