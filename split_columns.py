#importing required packages
import pandas as pd

def splitTransform(inputFile, columnName, outputFile, sIncludeCol, delimiter, delimBtw, Byposition, Posbtw, PosInt):
    dataset = pd.read_csv(inputFile) # reading as pandas dataframe
    columnsToInclude = [columnName]
    data = []
    if sIncludeCol:
        for y in sIncludeCol:
            if y.lower() != "all":
                columnsToInclude.append(y)                       #adding additional columns for the case "all" columns
    # Standardize all required columns
    data_columns = list(dataset.columns.values)
    # if delimiter and Byposition :
    #     return "Please select only one parameter"
    try:
        if (sIncludeCol and sIncludeCol[0].lower() != "all") or (not sIncludeCol):
            dataset = dataset[columnsToInclude]
            data_columns = columnsToInclude
        new_data_list = []
        new_data = ""
        new_col_length = 0
        if delimiter and len(delimiter) == 1:
            new_data = dataset[columnName].str.split("".join(delimiter), expand=True)
            new_data_list = new_data.columns.values
            new_col_length = len(new_data_list)
        else:
            append_data =  new_data_list.append
            for i in dataset[columnName].values.tolist():
                i = str(i)
                if delimiter:
                    append_data(split_delimiter(i, delimiter))
                elif delimBtw:
                    append_data(split_delimBtw(i, delimBtw))
                elif Byposition:
                    # if max(Byposition) > len(i):
                    #     raise Exception("The provided split condition is not consistent with the length of the record")
                    append_data(split_position(i, Byposition))
                elif Posbtw:
                    # if max(Posbtw) > len(i):
                    #     raise Exception("The provided split condition is not consistent with the length of the record")
                    append_data(split_btw_position(i, Posbtw))
                else:
                    # if PosInt[0] * PosInt[1] > len(i):
                    #     raise Exception("The Input values exceed the length of the record in the column" )
                    append_data(split_interval_position(i, PosInt))

            new_col_length = len(new_data_list[0])
        col_index = data_columns.index(columnName)
        new_col = []
        for num in range(new_col_length):
            col_index += 1
            new_column_name = columnName + "_" + str(num+1)
            new_col.append(new_column_name)
            data_columns.insert(col_index, new_column_name)
        if (delimiter or delimBtw or Byposition or Posbtw or PosInt) and len(delimiter) != 1:
            new_data = pd.DataFrame(new_data_list, columns=new_col)
        dataset[new_col] = new_data
        dataset = dataset[data_columns]
        dataset.to_csv(outputFile, index=False)
    except Exception as e:
         print(str(e))
         return "Split process failed due to exception: " + str(e)

def split_delimiter(s1, delimiters):
    new_data_list = []
    append_new_data = new_data_list.append
    for delimiter in delimiters:
        new_str = ""        ##  2012:123:-12-08T2222
        for ind,character in enumerate(s1):
            if delimiter == character:
                append_new_data(new_str)
                s1 = s1[ind+1:]
                break
            new_str += character
    if s1:
        append_new_data(s1)
    return new_data_list

def split_position(s1, positions):
    new_data_list = []
    ind = 0
    for p in positions:
        new_data_list.append(s1[ind:p])
        ind=p
    if not ind == len(s1)-1:
        new_data_list.append(s1[ind:])

    return new_data_list

def split_delimBtw(s1, delimiters):
    new_data_list = []
    tmp_string = ""
    delimit_found = False
    delimit_done = False
    for character in s1:
        if character == delimiters[0] and not delimit_done:
            if tmp_string:
                new_data_list.append(tmp_string)
            tmp_string = ""
            delimit_found = True
        elif character == delimiters[1] and delimit_found:
            delimit_found = False
            delimit_done = True
            continue
        if not delimit_found:
            tmp_string+=character

    if tmp_string:
        new_data_list.append(tmp_string)
    # print(new_data_list)
    return new_data_list

def split_btw_position(s1, positions):
    new_data_list = []
    new_data_list.append(s1[:positions[0]])
    new_data_list.append(s1[positions[1]:])
    return new_data_list

def split_interval_position(s1, positions):  # POSXPOSYPOSZ  3 2  O/P" POS, XPO, SYP
    new_data_list = []
    appends_data = new_data_list.append
    ind = 0
    end = positions[0]
    for i in range(positions[1]):
        appends_data(s1[ind:end])
        ind = end
        end += positions[0]
    if positions[0]*positions[1] != len(s1):
        appends_data(s1[ind:])
    return new_data_list

#splitTransform("TestDataset.csv", "Position", "Finalresult123.csv", ["buyer"],[], [], [], [], [3,7])

# if recurring delimiters are present in a record, they should be mentioned in the list.
#["-", "-", "T", ":", ":", "/"]