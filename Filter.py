from pyspark.sql import SparkSession
import pandas as pd
import pyspark.sql.functions as f

spark = SparkSession.builder.getOrCreate()
print("SparkSession Starter")


def filterProcess(fileName, expression, outputFile):
    df = spark.read.option("header", True).csv(fileName)
    df = df.filter(expression)
    dataset = df.toPandas()  # converting to pandas dataframe
    dataset.to_csv(outputFile)


#filterProcess("IRIS.csv", "species like '%setosa%'", "IRISoutput.csv")
