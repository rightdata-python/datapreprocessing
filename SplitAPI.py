import uvicorn
import fastapi
from typing import List, Optional
from fastapi import FastAPI, Query
from pydantic import BaseModel

from split_columns import splitTransform
app = FastAPI()


class SplitTransform(BaseModel):
    # Input file
    inputFile: str
    # Column Name
    columnName: str
    # Output file storage location
    outputFile: "str"
    # additional columns to add if needed
    sIncludeCol: Optional[list]
    # parameter for single or multiple delimiter
    delimiter: Optional[list]
    delimBtw: Optional[list]
    # parameter for split by position
    Byposition: Optional[list]
    #parameter for split between position
    Posbtw: Optional[list]
    # parameter for split at regular interval
    PosInt:Optional[list]

@app.post("/SplitAPI")
# calling the functions with given inputs
async def SplitTransformAPI(LR1:SplitTransform):
    Result = splitTransform(LR1.inputFile, LR1.columnName, LR1.outputFile, LR1.sIncludeCol, LR1.delimiter, LR1.delimBtw, LR1.Byposition, LR1.Posbtw, LR1.PosInt)
    # ...
    # Runs the splitTransform
    # ...
    return {f"{Result}"}


"""
{
  "inputFile": "C:\\Users\\nikhi\\OneDrive\\Desktop\\SplitTransform\\TestDataset.csv",
  "columnName": "PositionInt",
  "outputFile": "C:\\Users\\nikhi\\OneDrive\\Desktop\\SplitTransform\\APIResult.csv",
  "sIncludeCol": [
    "all"
  ],
  "delimiter": [
    
  ],
  "delimBtw": [
    
  ],
  "Byposition": [
    
  ],
  "Posbtw": [
    4, 8
  ],
  "PosInt": [
    
  ]
}
"""
